<?php


class PostController extends Controller
{
    public function actionIndex()
    {
        $posts = Model::retrieveFromDb();
        $this->view->render(
            'post/index',
            ['posts' => $posts]
        );
    }
}