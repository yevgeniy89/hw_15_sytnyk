<?php


class ArticleController extends Controller
{
    public function actionIndex()
    {
        $articles = Article::retrieveFromDb();
        $this->view->render(
            'article/index',
            ['articles' => $articles]
        );
    }
}
