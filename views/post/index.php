<h2 class="display-4 py-4">News</h2>
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Short description</th>
        <th scope="col">Full description</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($posts as $id =>$post): ?>
        <tr>
            <th scope="row"><?= $post['id'] ?></th>
            <td><?= $post['short'] ?></td>
            <td><a href="<?=$id?>"><?= $post['description'] ?></a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>