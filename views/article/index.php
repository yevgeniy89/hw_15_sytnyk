<h2 class="display-4 py-4">Articles</h2>
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Title</th>
        <th scope="col">Text</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($articles as $id => $article): ?>
        <tr>
            <th scope="row"><?= $article['id'] ?></th>
            <td><?= $article['title'] ?></td>
            <td><a href="/article/index/<?= $id ?>"><?= $article['text'] ?></a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>