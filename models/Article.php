<?php


class Article
{
    protected $db;

    public function __construct()
    {
        try {
            $this->db = new PDO(
                'mysql:host=localhost:3306; dbname=members',
                'user',
                '123456'
            );
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = 'INSERT INTO articles SET title = \'Yahoo news\', text = \'Breaking news\' ';
            $this->db->exec($sql);
        } catch (PDOException $e) {
            echo $e . 'DB error';
        }
    }

    public static function retrieveFromDb()
    {
        $db = new Article();
        $sql = 'SELECT * FROM articles';
        $query = $db->db->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}