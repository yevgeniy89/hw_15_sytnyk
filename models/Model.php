<?php


class Model
{
    protected $db;

    public function __construct()
    {
        try {
            $this->db = new PDO(
                'mysql:host=localhost:3306; dbname=members',
                'user',
                '123456'
            );
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//            $sql = 'create table posts (
//		            id int not null auto_increment primary key,
//		            short varchar(255),
//		            description text
//	);
//';
//            $this->db->exec($sql);
//
//            $sql = 'CREATE TABLE articles (
//                    id int not null auto_increment primary key,
//		            title varchar(255),
//		            text text
//)';
//            $this->db->exec($sql);
            $sql = 'INSERT INTO posts SET short = \'Google news\', description = \'Breaking news\' ';
            $this->db->exec($sql);
        } catch (PDOException $e) {
            echo $e . 'DB error';
        }
    }

    public static function retrieveFromDb()
    {
        $db = new Model();
        $sql = 'SELECT * FROM posts';
        $query = $db->db->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}